<?php

$hasil = "";
function tentukan_nilai($number){
    //  kode disini
    switch ($number) {
        case ($number >= 85 && $number <=100):
            $hasil = 'Sangat Baik';
            break;
        case ($number >= 70 && $number < 85):
            $hasil = 'Baik';
            break;
        case ($number >= 60 && $number < 70):
            $hasil = 'Cukup';
            break;
        default:
            $hasil = 'Kurang';
            break;
    };
    return $hasil."<br>";
};

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>